package io.bitbucket.koopersmith.xsandos.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Test;

import io.bitbucket.koopersmith.xsandos.model.Move;
import io.bitbucket.koopersmith.xsandos.test.qt.WithXsAndOsQuickTheories;

public class MoveTest implements WithXsAndOsQuickTheories {

    @Test
    public void testMoveEqualityIsBasedOffCoordMarkerCombination() {
        qt().forAll(coordinates().all(), markers().all())
                .checkAssert((coord, marker) -> assertEquals(
                        Move.of(coord, marker),
                        Move.of(coord, marker)));
    }

}
