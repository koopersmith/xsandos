package io.bitbucket.koopersmith.xsandos.model.board;

import static io.bitbucket.koopersmith.xsandos.model.State.DRAW;
import static io.bitbucket.koopersmith.xsandos.model.State.EMPTY;
import static io.bitbucket.koopersmith.xsandos.model.State.PLAYING;
import static io.bitbucket.koopersmith.xsandos.model.State.VICTORY;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.Test;
import org.quicktheories.dsl.TheoryBuilder2;

import io.bitbucket.koopersmith.xsandos.model.Coordinate;
import io.bitbucket.koopersmith.xsandos.model.Marker;
import io.bitbucket.koopersmith.xsandos.model.board.Board;
import io.bitbucket.koopersmith.xsandos.model.exceptions.CoordinateOccupiedException;
import io.bitbucket.koopersmith.xsandos.model.exceptions.OutOfTurnException;
import io.bitbucket.koopersmith.xsandos.test.qt.WithXsAndOsQuickTheories;

public class BoardTest implements WithXsAndOsQuickTheories {

    @Test
    public void testGetNewEmptyBoard() {
        final Board emptyBoard = Board.getEmpty();
        assertNotNull(emptyBoard);
    }

    @Test
    public void testNewEmptyBoardsAreNotSameBoard() {
        final Board boardA = Board.getEmpty();
        final Board boardB = Board.getEmpty();
        assertFalse(boardA.isSameBoardAs(boardB));
    }

    @Test
    public void testNewEmptyBoardsAreNotEqual() {
        final Board boardA = Board.getEmpty();
        final Board boardB = Board.getEmpty();
        assertNotEquals(boardA, boardB);
    }

    @Test
    public void testSameMoveIsAlwaysEqual() {
        final Board board = Board.getEmpty();
        forAllCoordinateMarkerCombinations().checkAssert(
                (coord, marker) -> assertEquals(
                        board.place(coord, marker),
                        board.place(coord, marker)));
    }

    @Test
    public void testBeforeAndAfterMoveAreSameBoard() {
        final Board board = Board.getEmpty();
        forAllCoordinateMarkerCombinations().check(
                (coord, marker) -> board
                        .place(coord, marker)
                        .isSameBoardAs(board));
    }

    @Test
    public void testCannotPlaceTwoOfSameMarkerInARow() {
        qt().forAll(coordinates().all(), coordinates().all(), markers().all())
                .assuming((coordA, coordB, marker) -> coordA != coordB)
                .checkAssert((coordA, coordB, marker) -> assertThrows(
                        OutOfTurnException.class,
                        () -> Board.getEmpty()
                                .place(coordA, marker)
                                .place(coordB, marker)));
    }

    @Test
    public void testCannotPlaceTwoMarkersAtSameCoordinate() {
        forAllCoordinateMarkerCombinations()
                .checkAssert((coord, marker) -> assertThrows(
                        CoordinateOccupiedException.class,
                        () -> Board.getEmpty()
                                .place(coord, marker)
                                .place(coord, marker.getOther())));
    }

    @Test
    public void testEmptyState() {
        assertEquals(EMPTY, Board.getEmpty().getState());
    }

    @Test
    public void testPlayingStateAfterFirstMove() {
        forAllCoordinateMarkerCombinations().checkAssert(
                (coord, marker) -> assertEquals(
                        PLAYING,
                        Board.getEmpty()
                                .place(coord, marker)
                                .getState()));
    }

    @Test
    public void testPlayingStateAfterAny4Moves() {
        qt().forAll(moveSequences().with(
                markers().all(),
                integers().between(1, 4),
                coordinates().all())).checkAssert(
                        sequence -> assertEquals(
                                PLAYING,
                                sequence.applyTo(Board.getEmpty())
                                        .getState()));
    }

    @Test
    public void testRowVictoryStates() {
        for (int y = 0; y < Coordinate.MAX_VALUE; y++) {
            qt().forAll(moveSequences()
                    .victoryRow(y, markers().all(), markers().all()))
                    .checkAssert(moveSequence -> assertEquals(
                            VICTORY,
                            moveSequence
                                    .applyTo(Board.getEmpty())
                                    .getState()));
        }
    }

    @Test
    public void testColumnVictoryStates() {
        for (int x = 0; x < Coordinate.MAX_VALUE; x++) {
            qt().forAll(moveSequences()
                    .victoryColumn(x, markers().all(), markers().all()))
                    .checkAssert(moveSequence -> assertEquals(
                            VICTORY,
                            moveSequence
                                    .applyTo(Board.getEmpty())
                                    .getState()));
        }
    }

    @Test
    public void testDiagonalVictoryStates() {
        qt().forAll(moveSequences()
                .victoryDiagonal(markers().all(), markers().all()))
                .checkAssert(moveSequence -> assertEquals(
                        VICTORY,
                        moveSequence
                                .applyTo(Board.getEmpty())
                                .getState()));
    }

    @Test
    public void testUnDiagonalVictoryStates() {
        qt().forAll(moveSequences()
                .victoryUnDiagonal(markers().all(), markers().all()))
                .checkAssert(moveSequence -> assertEquals(
                        VICTORY,
                        moveSequence
                                .applyTo(Board.getEmpty())
                                .getState()));
    }

    @Test
    public void testDraw() {
        qt().forAll(moveSequences().endAtDraw(markers().all()))
                .checkAssert(moveSequence -> assertEquals(
                        DRAW, moveSequence
                                .applyTo(Board.getEmpty())
                                .getState()));
    }

    private TheoryBuilder2<Coordinate, Marker> forAllCoordinateMarkerCombinations() {
        return qt().forAll(coordinates().all(), markers().all());
    }

}
