package io.bitbucket.koopersmith.xsandos.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.List;
import java.util.stream.Collectors;

import org.junit.Test;

import io.bitbucket.koopersmith.xsandos.model.Coordinate;
import io.bitbucket.koopersmith.xsandos.model.Marker;
import io.bitbucket.koopersmith.xsandos.model.Move;
import io.bitbucket.koopersmith.xsandos.model.MoveSequence;
import io.bitbucket.koopersmith.xsandos.model.exceptions.CoordinateOccupiedException;
import io.bitbucket.koopersmith.xsandos.model.exceptions.OutOfTurnException;
import io.bitbucket.koopersmith.xsandos.test.qt.WithXsAndOsQuickTheories;

public class MoveSequenceTest implements WithXsAndOsQuickTheories {

    @Test
    public void testSimpleEquality() {
        qt().forAll(coordinates().all(), markers().all())
                .checkAssert((coord, marker) -> assertEquals(
                        MoveSequence.from(Move.of(coord, marker)),
                        MoveSequence.from(Move.of(coord, marker))));
    }

    @Test
    public void testSimpleInequality() {
        qt().forAll(coordinates().all(), markers().all())
                .checkAssert((coord, marker) -> assertNotEquals(
                        MoveSequence.from(Move.of(coord, marker)),
                        MoveSequence.from(Move.of(coord, marker.getOther()))));
    }

    @Test
    public void testCannotCreateNewSequenceThatPlacesTwoMarkersAtSameCoordinate() {
        qt().forAll(coordinates().all(), markers().all())
                .checkAssert((coord, marker) -> assertThrows(
                        CoordinateOccupiedException.class,
                        () -> MoveSequence.from(
                                Move.of(coord, marker),
                                Move.of(coord, marker.getOther()))));
    }

    @Test
    public void testCannotCreateSequenceThatPlacesTwoMarkersAtSameCoordinate() {
        qt().forAll(coordinates().all(), markers().all())
                .checkAssert((coord, marker) -> assertThrows(
                        CoordinateOccupiedException.class, () -> {
                            final MoveSequence sequence = MoveSequence
                                    .from(Move.of(coord, marker));
                            sequence.add(Move.of(coord, marker.getOther()));
                        }));
    }

    @Test
    public void testCannotCreateNewSequenceThatPlacesTwoOfSameMarkerInARow() {
        qt().forAll(coordinates().all(),
                coordinates().all(),
                markers().all())
                .assuming((coordA, coordB, marker) -> coordA != coordB)
                .checkAssert((coordA, coordB, marker) -> assertThrows(
                        OutOfTurnException.class,
                        () -> MoveSequence.from(
                                Move.of(coordA, marker),
                                Move.of(coordB, marker))));
    }

    @Test
    public void testCannotCreateSequenceThatPlacesTwoOfSameMarkerInARow() {
        qt().forAll(coordinates().all(),
                coordinates().all(),
                markers().all())
                .assuming((coordA, coordB, marker) -> coordA != coordB)
                .checkAssert((coordA, coordB, marker) -> assertThrows(
                        OutOfTurnException.class, () -> {
                            final MoveSequence sequence = MoveSequence
                                    .from(Move.of(coordA, marker));
                            sequence.add(Move.of(coordB, marker));
                        }));
    }

    @Test
    public void testCreateMoveSequenceFromCoordinateListAndStartingMarker() {
        qt().withExamples(10000)
                .forAll(moveSequences().with(
                        markers().all(),
                        integers().between(1, Coordinate.values().length - 1),
                        coordinates().all()))
                .checkAssert(moveSequence -> {
                    final List<Coordinate> coords = moveSequence.stream()
                            .map(Move::getCoordinate)
                            .collect(Collectors.toList());
                    final Marker startMarker = moveSequence.getHead()
                            .map(Move::getMarker).get();
                    assertEquals(moveSequence, MoveSequence.from(coords, startMarker));
                });

    }

}
