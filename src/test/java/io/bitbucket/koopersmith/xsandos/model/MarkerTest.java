package io.bitbucket.koopersmith.xsandos.model;

import static io.bitbucket.koopersmith.xsandos.model.Marker.O;
import static io.bitbucket.koopersmith.xsandos.model.Marker.X;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Test;

import io.bitbucket.koopersmith.xsandos.test.qt.WithXsAndOsQuickTheories;

public class MarkerTest implements WithXsAndOsQuickTheories {

    @Test
    public void testXGetOtherIsO() {
        assertEquals(O, X.getOther());
    }

    @Test
    public void testOGetOtherIsX() {
        assertEquals(X, O.getOther());
    }

}
