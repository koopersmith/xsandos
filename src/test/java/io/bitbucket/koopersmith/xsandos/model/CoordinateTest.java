package io.bitbucket.koopersmith.xsandos.model;

import static org.junit.Assert.assertEquals;

import java.util.stream.Stream;

import org.junit.Test;
import org.quicktheories.WithQuickTheories;

import io.bitbucket.koopersmith.xsandos.model.Coordinate;

public class CoordinateTest implements WithQuickTheories {

    @Test
    public void testAllCoordinatesMatch() {
        Stream.of(Coordinate.values())
                .forEach(coord -> {
                    assertEquals(coord, Coordinate
                            .at(coord.getX(), coord.getY())
                            .get());
                });
    }

    @Test
    public void testAllValidCoordinatesArePresentExactlyOnce() {
        qt().forAll(
                integers().between(0, Coordinate.MAX_VALUE),
                integers().between(0, Coordinate.MAX_VALUE))
                .check((x, y) -> Stream.of(Coordinate.values())
                        .filter(coord -> coord.isAt(x, y))
                        .count() == 1);
    }

    @Test
    public void testAllPositiveInvalidCoordinatesAreNotPresent() {
        qt().forAll(
                integers().between(3, Integer.MAX_VALUE),
                integers().between(3, Integer.MAX_VALUE))
                .check((x, y) -> !Coordinate.at(x, y).isPresent());
    }

    @Test
    public void testAllNegativeCoordinatesAreNotPresent() {
        qt().forAll(
                integers().between(Integer.MIN_VALUE, -1),
                integers().between(Integer.MIN_VALUE, -1))
                .check((x, y) -> !Coordinate.at(x, y).isPresent());
    }

}
