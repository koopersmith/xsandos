package io.bitbucket.koopersmith.xsandos.test.qt;

import org.quicktheories.WithQuickTheories;

public interface WithXsAndOsQuickTheories extends WithQuickTheories {

    default MarkerDSL markers() {
        return new MarkerDSL();
    }

    default CoordinateDSL coordinates() {
        return new CoordinateDSL();
    }

    default MoveSequenceDSL moveSequences() {
        return new MoveSequenceDSL();
    }

}
