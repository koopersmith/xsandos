package io.bitbucket.koopersmith.xsandos.test.qt;

import org.quicktheories.core.Gen;
import org.quicktheories.generators.Generate;

import io.bitbucket.koopersmith.xsandos.model.Marker;

public final class MarkerDSL {

    protected MarkerDSL() {}

    public Gen<Marker> all() {
        return Generate.enumValues(Marker.class);
    }

}
