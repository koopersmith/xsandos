package io.bitbucket.koopersmith.xsandos.test.qt;

import org.quicktheories.core.Gen;
import org.quicktheories.generators.Generate;

import io.bitbucket.koopersmith.xsandos.model.Coordinate;

public final class CoordinateDSL {

    protected CoordinateDSL() {}

    public Gen<Coordinate> all() {
        return Generate.enumValues(Coordinate.class);
    }

}
