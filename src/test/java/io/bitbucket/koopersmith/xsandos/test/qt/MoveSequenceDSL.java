package io.bitbucket.koopersmith.xsandos.test.qt;

import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import org.quicktheories.core.Gen;
import org.quicktheories.core.RandomnessSource;
import org.quicktheories.impl.Constraint;

import io.bitbucket.koopersmith.xsandos.model.Coordinate;
import io.bitbucket.koopersmith.xsandos.model.Marker;
import io.bitbucket.koopersmith.xsandos.model.Move;
import io.bitbucket.koopersmith.xsandos.model.MoveSequence;
import io.bitbucket.koopersmith.xsandos.model.State;

public final class MoveSequenceDSL {

    protected MoveSequenceDSL() {}

    public Gen<MoveSequence> with(Gen<Marker> starterGen, Gen<Integer> numberOfMovesGen,
            Gen<Coordinate> coordinateGen) {
        return random -> {
            final int numberOfMoves = numberOfMovesGen.generate(random);
            final List<Coordinate> spentCoords = new ArrayList<>();
            final List<Move> moves = new ArrayList<>();
            Marker marker = starterGen.generate(random);
            for (int i = 0; i < numberOfMoves; i++) {
                Coordinate coord;
                do {
                    coord = coordinateGen.generate(random);
                }
                while (spentCoords.size() < Coordinate.COUNT - 1
                        && spentCoords.contains(coord));
                spentCoords.add(coord);
                moves.add(Move.of(coord, marker));
                marker = marker.getOther();
            }
            return MoveSequence.from(moves);
        };
    }

    public Gen<MoveSequence> victoryColumn(int x, Gen<Marker> starterGen, Gen<Marker> winnerGen) {
        return victory(() -> new ArrayList<>(Coordinate.getColumn(x)), starterGen, winnerGen);
    }

    public Gen<MoveSequence> victoryRow(int y, Gen<Marker> starterGen, Gen<Marker> winnerGen) {
        return victory(() -> new ArrayList<>(Coordinate.getRow(y)), starterGen, winnerGen);
    }

    public Gen<MoveSequence> victoryDiagonal(Gen<Marker> starterGen, Gen<Marker> winnerGen) {
        return victory(() -> new ArrayList<>(Coordinate.getDiagonal()), starterGen, winnerGen);
    }

    public Gen<MoveSequence> victoryUnDiagonal(Gen<Marker> starterGen, Gen<Marker> winnerGen) {
        return victory(() -> new ArrayList<>(Coordinate.getUnDiagonal()), starterGen, winnerGen);
    }

    private static final Gen<MoveSequence> victory(Supplier<List<Coordinate>> victoryCoordsSupplier,
            Gen<Marker> starterGen, Gen<Marker> winnerGen) {
        return random -> {
            final List<Coordinate> victoryCoords = victoryCoordsSupplier.get();
            final List<Coordinate> otherCoords = getAllCoordsExcept(victoryCoords);
            final List<Move> moves = new ArrayList<>();
            final Marker winner = starterGen.generate(random);
            Marker marker = starterGen.generate(random);
            while (!victoryCoords.isEmpty()) {
                final List<Coordinate> choices = (marker == winner) ? victoryCoords : otherCoords;
                if (choices.isEmpty()) break;
                final Coordinate coord = getFrom(random, choices);
                moves.add(Move.of(coord, marker));
                marker = marker.getOther();
            }
            return MoveSequence.from(moves);
        };
    }

    private static List<Coordinate> getAllCoordsExcept(List<Coordinate> exceptions) {
        return Coordinate.streamAll()
                .filter(coord -> !exceptions.contains(coord))
                .collect(toList());
    }

    private static Coordinate getFrom(RandomnessSource random, List<Coordinate> choices) {
        final int maxIndex = choices.size() - 1;
        final int index = (int) random.next(Constraint.between(0, maxIndex));
        final Coordinate choice = choices.get(index);
        choices.remove(choice);
        return choice;
    }

    public Gen<MoveSequence> endAtDraw(Gen<Marker> starterGen) {
        return random -> {
            Marker marker = starterGen.generate(random);
            MoveSequence sequence = MoveSequence.getEmpty();
            while (sequence.getState() == State.EMPTY
                    || sequence.getState() == State.PLAYING) {
                sequence = addNonWinningMove(random, sequence, marker);
                marker = marker.getOther();
                if (sequence.getState() == State.VICTORY) {
                    sequence = MoveSequence.getEmpty();
                }
            }
            return sequence;
        };
    }

    private static MoveSequence addNonWinningMove(RandomnessSource random, MoveSequence sequence, Marker marker) {
        final List<Coordinate> choices = getAllCoordsExcept(
                sequence.stream().map(Move::getCoordinate).collect(Collectors.toList()));
        MoveSequence newSequence;
        do {
            final Coordinate coord = getFrom(random, choices);
            newSequence = sequence.add(Move.of(coord, marker));
        }
        while (newSequence.getState() == State.VICTORY && !choices.isEmpty());
        return newSequence;
    }

}
