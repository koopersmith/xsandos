package io.bitbucket.koopersmith.xsandos.view.javafx.startscreen;

import io.bitbucket.koopersmith.xsandos.view.javafx.game.GameScene;
import io.bitbucket.koopersmith.xsandos.view.javafx.scenes.CommonScene;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class StartScene extends CommonScene<BorderPane> {

    public StartScene(double width, double height) {
        super(new BorderPane(), width, height);
    }

    @Override
    protected void addCustomPieces(BorderPane root) {
        final Button button = new Button("Start");
        button.setOnAction(this::gotoGameScene);
        root.setCenter(button);
    }

    private void gotoGameScene(ActionEvent event) {
        getStage().setScene(getGameScene());
    }

    public Stage getStage() {
        return (Stage) getWindow();
    }

    private GameScene getGameScene() {
        return new GameScene(getWidth(), getHeight());
    }

}
