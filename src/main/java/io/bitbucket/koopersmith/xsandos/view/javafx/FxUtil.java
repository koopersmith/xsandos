package io.bitbucket.koopersmith.xsandos.view.javafx;

import java.util.Optional;

import javafx.event.ActionEvent;
import javafx.scene.Node;
import javafx.stage.Stage;

public class FxUtil {

    public static Optional<Stage> getStage(ActionEvent event) {
        final Object source = event.getSource();
        if (source instanceof Node) {
            final Node node = (Node) source;
            return Optional.of((Stage) node.getScene().getWindow());
        }
        return Optional.empty();
    }

}
