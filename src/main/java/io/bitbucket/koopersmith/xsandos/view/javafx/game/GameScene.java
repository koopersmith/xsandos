package io.bitbucket.koopersmith.xsandos.view.javafx.game;

import io.bitbucket.koopersmith.xsandos.view.javafx.scenes.CommonScene;

public class GameScene extends CommonScene<CoordinatePane> {

    public GameScene(double width, double height) {
        super(new CoordinatePane(), width, height);
    }

    @Override
    protected void addCustomPieces(CoordinatePane root) {}

}
