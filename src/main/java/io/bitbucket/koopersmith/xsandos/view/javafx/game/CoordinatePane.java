package io.bitbucket.koopersmith.xsandos.view.javafx.game;

import io.bitbucket.koopersmith.xsandos.model.Coordinate;
import javafx.scene.Node;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Region;

public class CoordinatePane extends BorderPane {

    private static final int GRID_SIZE = 3;

    public CoordinatePane() {
        setCenter(createGrid());
    }

    private Node createGrid() {
        final GridPane grid = new GridPane();
        grid.setGridLinesVisible(true);
        Coordinate.streamAll().forEach(coord -> createButton(coord, grid));
        return grid;
    }

    private CoordinateButton createButton(Coordinate coord, GridPane grid) {
        final CoordinateButton button = new CoordinateButton(coord);
        bindChildToParentDimension(button, grid);
        grid.add(button, coord.getX(), coord.getY());
        return button;
    }

    private static void bindChildToParentDimension(Region child, Region parent) {
        child.prefWidthProperty().bind(parent.widthProperty().divide(GRID_SIZE));
        child.prefHeightProperty().bind(parent.heightProperty().divide(GRID_SIZE));
    }

}
