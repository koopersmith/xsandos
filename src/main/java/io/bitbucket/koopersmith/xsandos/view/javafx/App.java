package io.bitbucket.koopersmith.xsandos.view.javafx;

import io.bitbucket.koopersmith.xsandos.view.javafx.startscreen.StartScene;
import javafx.application.Application;
import javafx.stage.Stage;

public class App extends Application {

    private static final String TITLE = "X's and O's Game";

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage window) throws Exception {
        window.setTitle(TITLE);
        window.setScene(new StartScene(320, 240));
        window.show();
    }

}
