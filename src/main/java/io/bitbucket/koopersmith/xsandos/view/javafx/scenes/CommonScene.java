package io.bitbucket.koopersmith.xsandos.view.javafx.scenes;

import javafx.scene.Parent;
import javafx.scene.Scene;

public abstract class CommonScene<T extends Parent> extends Scene {

    private static final String DEFAULT_CSS = "/javafx/style.css";

    public CommonScene(T root) {
        super(root);
        init(root);
    }

    public CommonScene(T root, double width, double height) {
        super(root, width, height);
        init(root);
    }

    private void init(T root) {
        root.getStylesheets().add(DEFAULT_CSS);
        addCustomPieces(root);
    }

    protected abstract void addCustomPieces(T root);

}
