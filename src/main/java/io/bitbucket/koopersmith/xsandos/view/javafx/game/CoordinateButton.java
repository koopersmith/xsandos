package io.bitbucket.koopersmith.xsandos.view.javafx.game;

import java.util.Optional;

import io.bitbucket.koopersmith.xsandos.model.Coordinate;
import io.bitbucket.koopersmith.xsandos.model.Marker;
import javafx.beans.property.ObjectPropertyBase;
import javafx.beans.property.Property;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.ReadOnlyProperty;
import javafx.scene.control.Button;

public class CoordinateButton extends Button {

    private final ReadOnlyProperty<Coordinate> coordProperty;
    private final Property<Marker> markerProperty;

    public CoordinateButton(Coordinate coord) {
        super(coord.toString());
        this.coordProperty = new ReadOnlyObjectWrapper<Coordinate>(coord);
        this.markerProperty = new MarkerProperty(this);
    }

    private class MarkerProperty extends ObjectPropertyBase<Marker> {

        private final Object bean;

        private MarkerProperty(Object bean) {
            this.bean = bean;
        }

        @Override
        public Object getBean() {
            return bean;
        }

        @Override
        public String getName() {
            return "Marker";
        }

    }

    public Coordinate getCoordinate() {
        return coordinate().getValue();
    }

    public ReadOnlyProperty<Coordinate> coordinate() {
        return coordProperty;
    }

    public Optional<Marker> getMarker() {
        return Optional.ofNullable(marker().getValue());
    }

    public void setMarker(Marker marker) {
        marker().setValue(marker);
    }

    public Property<Marker> marker() {
        return markerProperty;
    }

}
