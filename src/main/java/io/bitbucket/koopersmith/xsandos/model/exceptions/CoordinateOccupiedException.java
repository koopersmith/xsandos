package io.bitbucket.koopersmith.xsandos.model.exceptions;

import io.bitbucket.koopersmith.xsandos.model.Coordinate;

public class CoordinateOccupiedException extends IllegalMoveException {

    private static final String MESSAGE = "Cannot place marker at %s, the space is already occupied.";

    public CoordinateOccupiedException(Coordinate coord) {
        super(String.format(MESSAGE, coord));
    }

}
