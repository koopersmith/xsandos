package io.bitbucket.koopersmith.xsandos.model.exceptions;

public class IllegalMoveException extends RuntimeException {

    public IllegalMoveException(String message) {
        super(message);
    }

}
