package io.bitbucket.koopersmith.xsandos.model;

public enum Marker {

    X {

        @Override
        public Marker getOther() {
            return O;
        }

    },

    O {

        @Override
        public Marker getOther() {
            return X;
        }

    };

    public abstract Marker getOther();

}
