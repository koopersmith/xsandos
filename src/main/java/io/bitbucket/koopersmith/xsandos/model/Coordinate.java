package io.bitbucket.koopersmith.xsandos.model;

import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

public enum Coordinate {

    A1(0, 0), B1(1, 0), C1(2, 0), //
    A2(0, 1), B2(1, 1), C2(2, 1), //
    A3(0, 2), B3(1, 2), C3(2, 2); //

    public static final int MAX_VALUE = 2;
    public static final int COUNT = values().length;

    private final int x;
    private final int y;

    private Coordinate(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public static Optional<Coordinate> at(int x, int y) {
        return streamAll()
                .filter(coord -> coord.isAt(x, y))
                .findFirst();
    }

    public static List<Coordinate> getColumn(int x) {
        return streamAll()
                .filter(c -> c.x == x)
                .collect(toList());
    }

    public static List<Coordinate> getRow(int y) {
        return streamAll()
                .filter(c -> c.y == y)
                .collect(toList());
    }

    public static List<Coordinate> getDiagonal() {
        return streamAll()
                .filter(c -> c.x == c.y)
                .collect(toList());
    }

    public static List<Coordinate> getUnDiagonal() {
        return streamAll()
                .filter(c -> c.x + c.y == MAX_VALUE)
                .collect(toList());
    }

    public static Stream<Coordinate> streamAll() {
        return Stream.of(values());
    }

    public boolean isAt(int x, int y) {
        return this.x == x && this.y == y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public List<Coordinate> getColumn() {
        return getColumn(x);
    }

    public List<Coordinate> getRow() {
        return getRow(y);
    }

}
