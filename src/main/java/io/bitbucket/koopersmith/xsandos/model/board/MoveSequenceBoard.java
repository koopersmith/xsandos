package io.bitbucket.koopersmith.xsandos.model.board;

import java.util.Objects;
import java.util.Optional;

import io.bitbucket.koopersmith.xsandos.model.Coordinate;
import io.bitbucket.koopersmith.xsandos.model.Marker;
import io.bitbucket.koopersmith.xsandos.model.Move;
import io.bitbucket.koopersmith.xsandos.model.MoveSequence;
import io.bitbucket.koopersmith.xsandos.model.State;
import io.bitbucket.koopersmith.xsandos.model.exceptions.OutOfTurnException;

public final class MoveSequenceBoard implements Board {

    private final long id;
    private final MoveSequence sequence;

    protected MoveSequenceBoard(long id, Move firstMove) {
        this(id, MoveSequence.from(firstMove));
    }

    private MoveSequenceBoard(long id, MoveSequence sequence) {
        this.id = id;
        this.sequence = sequence;
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public State getState() {
        return sequence.getState();
    }

    @Override
    public boolean isTaken(Coordinate coord) {
        return sequence.stream()
                .map(Move::getCoordinate)
                .filter(c -> c.equals(coord))
                .findFirst()
                .isPresent();
    }

    @Override
    public Optional<Marker> read(Coordinate coord) {
        return sequence.stream()
                .filter(move -> move.getCoordinate().equals(coord))
                .map(Move::getMarker)
                .findFirst();
    }

    @Override
    public Board place(Coordinate coord, Marker marker) throws OutOfTurnException {
        return new MoveSequenceBoard(id, sequence.add(Move.of(coord, marker)));
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, sequence);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj instanceof MoveSequenceBoard) {
            final MoveSequenceBoard other = (MoveSequenceBoard) obj;
            return id == other.id
                    && sequence.equals(other.sequence);
        }
        return false;
    }

}
