package io.bitbucket.koopersmith.xsandos.model;

public enum State {

    EMPTY, PLAYING, VICTORY, DRAW;

}
