package io.bitbucket.koopersmith.xsandos.model.exceptions;

import io.bitbucket.koopersmith.xsandos.model.Marker;

public class OutOfTurnException extends IllegalMoveException {

    private static final String MESSAGE = "Cannot place %s on %s turn.";

    public OutOfTurnException(Marker placed, Marker expected) {
        super(String.format(MESSAGE, placed, expected));
    }

}
