package io.bitbucket.koopersmith.xsandos.model.board;

import java.util.Optional;

import io.bitbucket.koopersmith.xsandos.model.Coordinate;
import io.bitbucket.koopersmith.xsandos.model.Marker;
import io.bitbucket.koopersmith.xsandos.model.State;

public interface Board {

    public static Board getEmpty() {
        return new EmptyBoard(MoveSequenceBoard::new);
    }

    long getId();

    default boolean isSameBoardAs(Board other) {
        return other != null && getId() == other.getId();
    }

    State getState();

    boolean isTaken(Coordinate coord);

    Optional<Marker> read(Coordinate coord);

    Board place(Coordinate coord, Marker marker);

}