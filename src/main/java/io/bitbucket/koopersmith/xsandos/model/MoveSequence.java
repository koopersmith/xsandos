package io.bitbucket.koopersmith.xsandos.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import io.bitbucket.koopersmith.xsandos.model.board.Board;
import io.bitbucket.koopersmith.xsandos.model.exceptions.CoordinateOccupiedException;
import io.bitbucket.koopersmith.xsandos.model.exceptions.OutOfTurnException;

public class MoveSequence implements Iterable<Move> {

    private final List<Move> sequence;
    private final State state;

    private MoveSequence(List<Move> moves) {
        this.sequence = moves;
        this.state = determineState();
        validate();
    }

    private void validate() {
        final Set<Coordinate> coordSet = new HashSet<>();
        Marker lastMarker = null;
        for (Move move : sequence) {
            final Coordinate coord = move.getCoordinate();
            if (coordSet.contains(coord)) {
                throw new CoordinateOccupiedException(coord);
            }
            coordSet.add(coord);
            final Marker marker = move.getMarker();
            if (lastMarker != null && lastMarker == marker) {
                throw new OutOfTurnException(marker, lastMarker.getOther());
            }
            lastMarker = marker;
        }
    }

    private State determineState() {
        final Optional<Move> lastMove = getTail();
        if (lastMove.isPresent()) {
            final Coordinate coord = lastMove.get().getCoordinate();
            final Marker marker = lastMove.get().getMarker();
            if (isVictory(coord.getRow().stream(), coord, marker)
                    || isVictory(coord.getColumn().stream(), coord, marker)
                    || isVictory(Coordinate.getDiagonal().stream(), coord, marker)
                    || isVictory(Coordinate.getUnDiagonal().stream(), coord, marker)) {
                return State.VICTORY;
            }
            if (sequence.size() == Coordinate.COUNT) {
                return State.DRAW;
            }
            return State.PLAYING;
        }
        return State.EMPTY;
    }

    private boolean isVictory(Stream<Coordinate> coordStream, Coordinate coord, Marker marker) {
        return coordStream.filter(c -> c != coord)
                .map(c -> sequence.stream().filter(move -> move.getCoordinate() == c).findFirst())
                .map(move -> (move.isPresent() ? move.get().getMarker() : null))
                .allMatch(m -> marker == m);
    }

    public static MoveSequence getEmpty() {
        return new MoveSequence(Collections.emptyList());
    }

    public static MoveSequence from(Move... moves) {
        return new MoveSequence(Arrays.asList(moves));
    }

    public static MoveSequence from(List<Coordinate> coords, Marker startMarker) {
        final List<Move> moves = new ArrayList<>();
        Marker marker = startMarker;
        for (Coordinate coord : coords) {
            moves.add(Move.of(coord, marker));
            marker = marker.getOther();
        }
        return new MoveSequence(Collections.unmodifiableList(moves));
    }

    public static MoveSequence from(List<Move> moves) {
        return new MoveSequence(getUnmodifiableCopy(moves));
    }

    private static List<Move> getUnmodifiableCopy(List<Move> moves, Move... newMoves) {
        final List<Move> copy = new ArrayList<>();
        copy.addAll(moves);
        copy.addAll(Arrays.asList(newMoves));
        return Collections.unmodifiableList(copy);
    }

    @Override
    public Iterator<Move> iterator() {
        return sequence.iterator();
    }

    public Stream<Move> stream() {
        return sequence.stream();
    }

    public MoveSequence add(Move move) {
        return new MoveSequence(getUnmodifiableCopy(sequence, move));
    }

    @Override
    public int hashCode() {
        return Objects.hash(state, sequence);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj instanceof MoveSequence) {
            final MoveSequence other = (MoveSequence) obj;
            return state == other.state
                    && sequence.equals(other.sequence);
        }
        return false;
    }

    public Board applyTo(Board board) {
        Board result = board;
        for (Move move : sequence) {
            result = result.place(move.getCoordinate(), move.getMarker());
        }
        return result;
    }

    public int size() {
        return sequence.size();
    }

    public State getState() {
        return state;
    }

    public Optional<Move> getHead() {
        return sequence.stream().findFirst();
    }

    public Optional<Move> getTail() {
        return sequence.stream().reduce((current, next) -> next);
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append("[")
                .append(sequence.stream()
                        .map(Move::toString)
                        .collect(Collectors.joining(", ")))
                .append("]")
                .toString();
    }

}
