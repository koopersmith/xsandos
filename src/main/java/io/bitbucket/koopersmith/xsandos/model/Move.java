package io.bitbucket.koopersmith.xsandos.model;

import java.util.Objects;

public class Move {

    private final Coordinate coord;
    private final Marker marker;

    private Move(Coordinate coord, Marker marker) {
        this.coord = coord;
        this.marker = marker;
    }

    public static Move of(Coordinate coord, Marker marker) {
        return new Move(coord, marker);
    }

    public Coordinate getCoordinate() {
        return coord;
    }

    public Marker getMarker() {
        return marker;
    }

    @Override
    public int hashCode() {
        return Objects.hash(coord, marker);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj instanceof Move) {
            final Move other = (Move) obj;
            return coord == other.coord
                    && marker == other.marker;
        }
        return false;
    }

    @Override
    public String toString() {
        return String.format("%s -> %s", marker, coord);
    }

}
