package io.bitbucket.koopersmith.xsandos.model.board;

import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.BiFunction;

import io.bitbucket.koopersmith.xsandos.model.Coordinate;
import io.bitbucket.koopersmith.xsandos.model.Marker;
import io.bitbucket.koopersmith.xsandos.model.Move;
import io.bitbucket.koopersmith.xsandos.model.State;
import io.bitbucket.koopersmith.xsandos.model.exceptions.OutOfTurnException;

public final class EmptyBoard implements Board {

    private static final long FIRST_ID = 0;
    private static final AtomicLong ID_GENERATOR = new AtomicLong(FIRST_ID);

    private final long id;
    private final BiFunction<Long, Move, Board> onFirstMove;

    protected EmptyBoard(BiFunction<Long, Move, Board> onFirstMove) {
        id = ID_GENERATOR.getAndIncrement();
        this.onFirstMove = onFirstMove;
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public State getState() {
        return State.EMPTY;
    }

    @Override
    public boolean isTaken(Coordinate coord) {
        return false;
    }

    @Override
    public Optional<Marker> read(Coordinate coord) {
        return Optional.empty();
    }

    @Override
    public Board place(Coordinate coord, Marker marker) throws OutOfTurnException {
        return onFirstMove.apply(id, Move.of(coord, marker));
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, State.EMPTY);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj instanceof EmptyBoard) {
            final EmptyBoard other = (EmptyBoard) obj;
            return isSameBoardAs(other);
        }
        if (obj instanceof Board) {
            final Board other = (Board) obj;
            return isSameBoardAs(other)
                    && other.getState() == State.EMPTY;
        }
        return false;
    }

}
